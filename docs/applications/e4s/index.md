# Extreme-scale Scientific Software Stack (E4S)

 The [Extreme-scale Scientific Software Stack (E4S)](https://e4s.readthedocs.io/en/latest/) is
 a curated software stack from the [Spack](https://spack.readthedocs.io/en/latest/) ecosystem that 
 is continuously built and tested on several pre-exascale systems. E4S is composed of many
 open-source projects, including xSDK, dev-tools, math-libraries, compilers, and more. 
 For a complete product list see [E4S Product Dictionary](https://e4s.readthedocs.io/en/latest/E4S_Products.html). 

E4S is shipped as a container (Docker, Singularity, Shifter, CharlieCloud), a [buildcache](https://oaciss.uoregon.edu/e4s/inventory.html),
or a Spack manifest (`spack.yaml`). Currently, we focus on building E4S from source using 
a `spack.yaml` file provided by the E4S team from https://github.com/E4S-Project/e4s. 

!!! note 
    We install as many packages from E4S provided in spack.yaml as possible. Some 
    packages were intentionally skipped such as specs tied to `develop` branches 
    or packages built for GPUs. Some additional packages couldn't be installed
    or had concretization issues with our base compiler.

## E4S Support Timeline

This table  outlines the support lifetime for each E4S version. The Release Date is 
marked on the day of E4S release when user documentation was live. Once E4S version has reached end of
support we will remove E4S and corresponding modulefiles and user documentation. As we approach the **End of Support** 
for a particular release, we will communicate via email and modulefile will include a banner when 
loading the module. 

**We recommend users to port their application and any scripts to the newer release.** 

| System |  Version | Release Date |  End of Support | 
| ------| --------- | ----------- | ---------------- |
| Cori | 20.10 | Jan 15th 2021 |  Mar 16th 2022 |
| Cori | 21.02 | Jun 11th 2021 | Mar 16th 2022 |
| Cori | 21.05 | Aug 23th 2021 | Oct 31st 2022 |
| Perlmutter | 21.11 | Jan 22nd 2022 | Mar 31st 2023 |
| Cori | 22.02 | Mar 16th 2022 | End of Life Cori (2023) |

## Point of Contact

If you need help with E4S you can submit a ticket at https://help.nersc.gov/. If you want to raise issue with
E4S team, please report them in the [E4S Github project](https://github.com/E4S-Project/e4s) which will be triaged to the
E4S team.

If you would like to see your package in E4S, please contact the E4S team:

- Michael A. Heroux (maherou@sandia.gov)

- Sameer Shende (sameer@cs.uoregon.edu)

## References

- E4S Home Page: https://e4s-project.github.io/
- E4S User Docs: https://e4s.readthedocs.io/en/latest/
- E4S GitHub: https://github.com/E4S-Project/e4s 
- Spack Infrastructure: https://software.nersc.gov/NERSC/spack-infrastructure 
- Spack Infrastructure Documentation: https://nersc-spack-infrastructure.rtfd.io/ 

## Past Events

 | Title | Date | Links |
 | ------- | ----- | ----- |
 | [E4S at DOE Facilities with Deep Dive at NERSC](https://www.exascaleproject.org/event/e4s_at_doe_100421/) | Oct 4th, 2021 | [Introduction to E4S](https://www.exascaleproject.org/wp-content/uploads/2021/08/E4S_100421_Shende.pdf)<br> [ECP Software Technology Overview](https://www.exascaleproject.org/wp-content/uploads/2021/08/E4S_100421_Heroux.pdf)<br> [Software Deployment at Facilities](https://www.exascaleproject.org/wp-content/uploads/2021/08/E4S_100421_Adamson.pdf)<br> [Recording](https://youtu.be/eSEiVxXpCDo) |
 