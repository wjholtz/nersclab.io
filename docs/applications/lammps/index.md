# LAMMPS

[LAMMPS](https://lammps.sandia.gov/) is a large scale classical
molecular dynamics code, and stands for Large-scale Atomic/Molecular Massively
Parallel Simulator.  LAMMPS has potentials for soft materials (biomolecules,
polymers), solid-state materials (metals, semiconductors) and coarse-grained or
mesoscopic systems. It can be used to model atoms or, more generically, as a
parallel particle simulator at the atomic, meso, or continuum scale.

## Availability and Supported Architectures

LAMMPS is available at NERSC as a [provided support level](https://docs.nersc.gov/policies/software-policy/) package.
LAMMPS runs at NERSC are currently supported on Haswell, KNL, and GPU nodes. 

## Application Information, Documentation and Support

The official LAMMPS is available at [LAMMPS Online Manual](https://lammps.sandia.gov/doc/Manual.html). 
LAMMPS has a large user base and a good user support. 
Question related to using LAMMPS can be posted to the [LAMMPS User forum](https://matsci.org/c/lammps/40). 
Archived [user mailing list](https://sourceforge.net/p/lammps/mailman/lammps-users/) are also useful to resolve 
some of the common user issues. 

!!! tip
    If *after* checking the above forum, if you believe that there is an issue
    with the NERSC module, please file a ticket with our [help
    desk](https://help.nersc.gov)

!!!

## Using LAMMPS at NERSC

NERSC uses [modules](../../environment/modules.md) to manage
access to software. To use the default version of LAMMPS, type:

```console
module load lammps
```

### Using LAMMPS on Cori

There are two ways of running LAMMPS on Cori: submitting a batch job, or
running interactively in an interactive batch session.

??? example "Sample Batch Script to Run LAMMPS on Cori Haswell"

    ```slurm
    #!/bin/bash
    #SBATCH -J test_lammps
    #SBATCH -C haswell
    #SBATCH -q debug
    #SBATCH -N 2
    #SBATCH -t 30:00
    #SBATCH -o test_lammps.o%j

    module load lammps

    srun -n 64 -c 2 --cpu-bind=cores lmp_cori -in test.in
    ```

??? example "Sample Batch Script to Run LAMMPS on Cori KNL"

    ```slurm
    #!/bin/bash
    #SBATCH -J test_lammps
    #SBATCH -C knl
    #SBATCH -q debug
    #SBATCH -N 2
    #SBATCH -t 30:00
    #SBATCH -o test_lammps.o%j

    module load lammps

    srun -n 136 -c 2 --cpu-bind=cores lmp_cori -in test.in
    ```

These job scripts request two nodes in the debug partition, and run for up to
30 minutes. The first example runs 64 MPI processes across 64 cores on 2 nodes
of Cori Haswell. The second example runs 136 MPI processes across 136 cores on
2 nodes of Cori KNL.

Then submit the job script using the sbatch command, e.g., assuming the job
script name is `test_lammps.slurm`:

```console
sbatch test_lammps.slurm
```

If running LAMMPS with Kokkos package, please review the [Kokkos package](https://docs.lammps.org/Speed_kokkos.html)
page to add appropriate flags to the job submission line as well as modify the input script.

## Building LAMMPS from source

Some users may be interested in building LAMMPS from source to enable more specific LAMMPS packages. 
The source files for LAMMPS can be [downloaded](https://www.lammps.org/download.html) as either a tar file 
or from the [LAMMPS Github repository](https://github.com/lammps/lammps). 

??? example "Building on Cori"
	The following procedure was used to build LAMMPS on Cori Haswell. 
    In the terminal:

    ```bash
    module purge
    module load esslurm
    module load gcc/11.2.0
    module load openmpi/4.1.2
    module load cmake

    git clone https://github.com/lammps/lammps.git
    cd lammps
    mkdir build
    cd build

    cmake -DCMAKE_INSTALL_PREFIX=$PWD/../install_hsw -DCMAKE_CXX_COMPILER=mpicxx \
                -DCMAKE_BUILD_TYPE=Release -D BUILD_MPI=yes -DKokkos_ENABLE_OPENMP=ON \
                -DKokkos_ARCH_HSW=ON -DCMAKE_CXX_STANDARD=17 -D PKG_MANYBODY=ON \
                -D PKG_MOLECULE=ON -D PKG_KSPACE=ON -D PKG_REPLICA=ON -D PKG_ASPHERE=ON \
                -D PKG_RIGID=ON -D PKG_KOKKOS=ON -D DOWNLOAD_KOKKOS=ON \
                -D CMAKE_POSITION_INDEPENDENT_CODE=ON -D CMAKE_EXE_FLAGS="-dynamic" ../cmake
    make -j16
    make install
    ```

??? example "Building on Perlmutter"
	The following procedure was used to build LAMMPS on Perlmutter GPU with Kokkos. 
    In the terminal:

    ```bash
    module load cudatoolkit
    module load craype-accel-nvidia80

    git clone https://github.com/lammps/lammps.git
    cd lammps
    mkdir build
    cd build

    cmake -DCMAKE_INSTALL_PREFIX=$PWD/../install_pm -D CMAKE_BUILD_TYPE=Release \
                -D CMAKE_Fortran_COMPILER=ftn -D CMAKE_C_COMPILER=cc -D CMAKE_CXX_COMPILER=CC \
                -D MPI_C_COMPILER=cc -D MPI_CXX_COMPILER=CC -D LAMMPS_EXCEPTIONS=ON \
                -D BUILD_SHARED_LIBS=ON -D PKG_KOKKOS=yes -D Kokkos_ARCH_AMPERE80=ON -D Kokkos_ENABLE_CUDA=yes \
                -D PKG_MANYBODY=ON -D PKG_MOLECULE=ON -D PKG_KSPACE=ON -D PKG_REPLICA=ON -D PKG_ASPHERE=ON \
                -D PKG_RIGID=ON -D PKG_MPIIO=ON \
                -D CMAKE_POSITION_INDEPENDENT_CODE=ON -D CMAKE_EXE_FLAGS="-dynamic" ../cmake
    make -j16
    make install
    ```

## Related Applications

* [NAMD](../namd/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
        Users are invited to contribute helpful information and corrections
        through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
